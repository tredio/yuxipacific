<?php

namespace YuxiPacificBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use YuxiPacificBundle\Repository\BooksRepository;

class BooksController extends Controller
{
    /**
     * returns book repository instance
     * @return BooksRepository
     */
    protected function getBooksRepository()
    {
        return $this->get('books.repository');
    }

    /**
     * @Route("/books/list", name="books_list")
     * @Template()
     */
    public function listAction()
    {
        $repository = $this->getBooksRepository();

        if($repository->isEmpty())
        {
            return $this->redirectToRoute('books_error');
        }

        return array();
    }

    /**
     * @Route("/books/error", name="books_error")
     * @Template()
     */
    public function booksErrorAction()
    {
        return array();
    }

    /**
     * @Template("@YuxiPacific/Books/bookTable.html.twig")
     * @return array
     */
    public function getAllAction()
    {
        $books = $this->getBooksRepository()->getAll();

        return array(
            'books' => $books
        );
    }

    /**
     * @Template("@YuxiPacific/Books/bookTable.html.twig")
     * @return array
     */
    public function getSpanishAction()
    {
        $spanishBooks = $this->getBooksRepository()->getByLanguage('Spanish');

        return array(
            'books' => $spanishBooks
        );
    }

    /**
     * @Template("@YuxiPacific/Books/bookTable.html.twig")
     * @return array
     */
    public function getFrenchAction()
    {
        $frenchBooks = $this->getBooksRepository()->getByLanguage('French');

        return array(
            'books' => $frenchBooks
        );
    }

    /**
     * @Template("@YuxiPacific/Books/bookTable.html.twig")
     * @param $operator
     * @param $value
     * @return array
     */
    public function getByPriceAction($operator, $value)
    {
        $books = $this->getBooksRepository()->getByPrice($operator, $value);

        return array(
            'books' => $books
        );
    }

    /**
     * @Template("@YuxiPacific/Books/bookTable.html.twig")
     * @param $operator
     * @param $value
     * @return array
     */
    public function getByQuantityAction($operator, $value)
    {
        $books = $this->getBooksRepository()->getByQuantity($operator, $value);

        return array(
            'books' => $books
        );
    }
}
