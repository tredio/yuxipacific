<?php

namespace YuxiPacificBundle\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use YuxiPacificBundle\Form\UploadXmlForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class XmlReaderController
 * @package YuxiPacificBundle\Controller
 */
class XmlReaderController extends Controller
{
    /**
     * Uploads xml and save result to user session
     * @Route("/xml-upload", name="upload_file")
     * @Template()
     */
    public function UploadFileAction()
    {
        $form = $this->createUploadForm();

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * process xml file and validates form
     * @Method("POST")
     * @Route("/xml-process", name="process_file")
     * @param Request $request
     * @Template("@App/XmlReader/UploadFile.html.twig")
     * @return array
     */
    public function ProcessUploadAction(Request $request)
    {
        $form = $this->createUploadForm();
        $form->handleRequest($request);

        if($form->isValid()) {
            $xmlProcessor = $this->get('xml.processor');
            /** @var UploadedFile $file */
            $file = $form->get('file')->getData();

            $books = $xmlProcessor->parseXmlToBooksArray(file_get_contents($file->getPathname()));

            $request->getSession()->set('books', $books);
            return $this->redirectToRoute('books_list');
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * prepares upload form for render
     * @return \Symfony\Component\Form\Form
     */
    protected function createUploadForm()
    {
        $form = $this->createForm( new UploadXmlForm(), null, array(
            'action' => $this->generateUrl("process_file"),
            'method' => 'POST'
        ));

        return $form;
    }
}
