<?php
/**
 * Created by PhpStorm.
 * User: Edgar González <edgarsys@gmail.com>
 * Date: 23/10/2015
 * Time: 10:57
 */

namespace YuxiPacificBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class UploadXmlForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'file', array(
            'label' => 'Select XML file',
            'constraints' => array(
                new File(array(
                    'maxSize' => 2000000,
                    'mimeTypes' => array(
                        'text/xml',
                        'application/xml'
                    ),
                ))
            )
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "upload_xml";
    }
}