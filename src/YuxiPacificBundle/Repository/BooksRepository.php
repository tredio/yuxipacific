<?php
/**
 * Created by PhpStorm.
 * User: Edgar González <edgarsys@gmail.com>
 * Date: 23/10/2015
 * Time: 15:47
 */

namespace YuxiPacificBundle\Repository;


use Pinq\Traversable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Exception\NotImplementedException;
use YuxiPacificBundle\Entity\Book;

class BooksRepository
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * gets books from session
     * @return mixed
     */
    protected function getBooks()
    {
        return $this->getRequest()->getSession()->get('books');
    }

    /**
     * saves arrays of books in session
     * @param array $books
     */
    public function saveBooks(array $books)
    {
        $this->getRequest()->getSession()->set('books', $books);
    }

    /**
     * determines if books have been loaded
     * @return bool
     */
    public function isEmpty()
    {
        return $this->getBooks() == null;
    }

    /**
     * gets all books
     * @return mixed
     */
    public function getAll()
    {
        return $this->getBooks();
    }

    /**
     * get books by language
     * @param $language
     * @return array
     */
    public function getByLanguage($language)
    {
        $allBooks = $this->getAll();

        $filtered = Traversable::from($allBooks)
            ->where(function(Book $book) use ($language){
                return $book->getLanguage() == $language;
            })
            ->asArray();

        return $filtered;
    }

    /**
     * get books by price
     * @param $operator
     * @param $value
     * @return array
     */
    public function getByPrice($operator, $value)
    {
        $allBooks = $this->getAll();

        $filtered = Traversable::from($allBooks)
            ->where(function(Book $book) use ($operator, $value){
                if($operator == '>=')
                {
                    return $book->getPrice() >= $value;
                }
                else
                {
                    throw new NotImplementedException("operator '$operator' not implemented");
                }
            })
            ->asArray();

        return $filtered;
    }

    /**
     * get books by quantity
     * @param $operator
     * @param $value
     * @return array
     */
    public function getByQuantity($operator, $value)
    {
        $allBooks = $this->getAll();

        $filtered = Traversable::from($allBooks)
            ->where(function(Book $book) use ($operator, $value){
                if($operator == '<')
                {
                    return $book->getQuantity() < $value;
                }
                else
                {
                    throw new NotImplementedException("operator '$operator' not implemented");
                }
            })
            ->asArray();

        return $filtered;
    }


}