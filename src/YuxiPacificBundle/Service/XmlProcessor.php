<?php
/**
 * Created by PhpStorm.
 * User: Edgar González <edgarsys@gmail.com>
 * Date: 23/10/2015
 * Time: 14:37
 */

namespace YuxiPacificBundle\Service;

use YuxiPacificBundle\Entity\Book;

class XmlProcessor
{

    /*
     * parse xml formatted books to an array of objects
     */
    public function parseXmlToBooksArray($xmlString)
    {
        $data = simplexml_load_string($xmlString);
        $books = array();

        foreach($data->Book as $bookData)
        {
            $book = new Book();
            $book->setQuantity(intval($bookData->Quantity))
                ->setTitle( (string)$bookData->Title)
                ->setPrice(floatval($bookData->Price))
                ->setLanguage((string)$bookData->Language)
                ->setAuthor((string)$bookData->Author)
                ->setCountry((string)$bookData->Country);
            $books[] = $book;
        }

        return $books;
    }

}